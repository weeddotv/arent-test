import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '@/styles/theme';
import Link from 'next/link';
import dayjs from 'dayjs';

const ColumnItemStyled = styled.div`
  figure {
    width: 100%;
    height: 144rem;
    position: relative;
    overflow: hidden;
    margin-bottom: 8rem;
    a {
      display: block;
      width: 100%;
      height: 100%;
      img {
        width: 100%;
        height: 100%;
        object-fit: cover;
        object-position: center;
        transition: 0.3s ease-in-out;
      }
    }
    p {
      position: absolute;
      left: 0;
      bottom: 0;
      font-size: 15rem;
      line-height: 24rem;
      padding: 0 8rem;
      background-color: ${colors.primary300};
    }
  }
  h3 {
    a {
      color: ${colors.dark500};
      font-size: 15rem;
      font-weight: 300;
      line-height: 22rem;
      letter-spacing: 0.075px;
      height: 48rem;
      font-family: 'Noto Sans JP', sans-serif;
      overflow: hidden;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      line-clamp: 2;
      -webkit-box-orient: vertical;
      transition: 0.3s ease-in-out;
    }
  }

  ul {
    li {
      color: ${colors.primary400};
      font-weight: 300;
      line-height: 22rem;
      letter-spacing: 0.06px;
      display: inline-block;
      margin-right: 8rem;
      font-family: 'Noto Sans JP', sans-serif;
      &:last-child {
        margin-right: 8rem;
      }
    }
  }

  &:hover {
    figure {
      a {
        img {
          transform: scale(1.04);
          transition: 0.3s ease-in-out;
        }
      }
    }
    h3{
      a{
        color: ${colors.primary300};
        transition: 0.3s ease-in-out;
      }
    }
  }
`;

const ColumnItem = ({ imageUrl, title, hashTags, date, url, ...rest }) => {
  return (
    <ColumnItemStyled {...rest}>
      <figure>
        <Link href={url}>
          <img src={imageUrl} alt={title} />
        </Link>

        <p>{dayjs(date).local().format('YYYY.MM.DD HH:mm')}</p>
      </figure>

      <h3>
        <Link href={url}>{title}</Link>
      </h3>
      <ul>
        {hashTags.map((item) => (
          <li key={item}>#{item}</li>
        ))}
      </ul>
    </ColumnItemStyled>
  );
};

ColumnItem.propTypes = {
  url: PropTypes.string,
  imageUrl: PropTypes.string,
  date: PropTypes.string,
  title: PropTypes.string,
  hashTags: PropTypes.array,
};

ColumnItem.defaultProps = {
  url: '#',
  imageUrl: '/images/d01.jpg',
  date: new Date(),
  title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
  hashTags: ['魚料理', '和食', 'DHA'],
};

export default memo(ColumnItem);
