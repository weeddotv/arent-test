import colors from '@/styles/theme';
import React, { memo } from 'react';
import styled from 'styled-components';

const ColumnHeadItemStyled = styled.div`
  padding: 24rem 8rem 22rem;
  background-color: ${colors.dark500};
  text-align: center;
  h3 {
    font-size: 22rem;
    font-weight: 400;
    line-height: 27rem;
    letter-spacing: 0.11px;
    color: ${colors.primary300};
    margin-bottom: 8rem;
    padding-bottom: 10rem;
    position: relative;
    &::after {
      content: '';
      position: absolute;
      background-color: white;
      left: 50%;
      bottom: 0;
      transform: translate(-50%, 0);
      height: 1px;
      width: 56rem;
    }
  }
  p {
    font-size: 18rem;
    font-weight: 300;
    line-height: 26rem;
    color: white;
    font-family: 'Noto Sans JP', sans-serif;
  }
`;

const ColumnHeadItem = ({ title, desc, ...rest }) => {
  return (
    <ColumnHeadItemStyled {...rest}>
      <h3>{title}</h3>
      <p>{desc}</p>
    </ColumnHeadItemStyled>
  );
};
ColumnHeadItem.propTypes = {};

ColumnHeadItem.defaultProps = {
  title: 'RECOMMENDED COLUMN',
  desc: 'オススメ',
};

export default memo(ColumnHeadItem);
