import dynamic from 'next/dynamic';

const CircleGraph = dynamic(() => import('./CircleGraph'), { ssr: false });
const LineGraph = dynamic(() => import('./LineGraph'), { ssr: false });

export { CircleGraph, LineGraph };
