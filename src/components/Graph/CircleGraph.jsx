import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import dayjs from 'dayjs';

const CircleGraphStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  svg {
    width: 181rem;
    height: 181rem;
  }

  h3 {
    color: white;
    font-family: 'Inter', sans-serif;
    font-size: 25rem;
    font-weight: 400;
    line-height: 30rem;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    span {
      font-size: 18rem;
      font-weight: 400;
      line-height: 22rem;
      font-family: inherit;
    }
  }
`;

const CircleGraph = ({ percent, date, ...rest }) => {
  return (
    <CircleGraphStyled {...rest}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="181"
        height="181"
        viewBox="0 0 181 181"
        fill="none"
      >
        <g filter="url(#filter0_d_0_94)">
          <path
            d="M90.5 181C40.598 181 0 140.402 0 90.5H3C3 138.748 42.252 178 90.5 178C138.748 178 178 138.748 178 90.5C178 42.252 138.748 3 90.5 3V0C140.402 0 181 40.598 181 90.5C181 140.402 140.402 181 90.5 181Z"
            fill="white"
          />
        </g>
        <defs>
          <filter
            id="filter0_d_0_94"
            x="-6"
            y="-6"
            width="193"
            height="193"
            filterUnits="userSpaceOnUse"
            colorInterpolationFilters="sRGB"
          >
            <feFlood floodOpacity="0" result="BackgroundImageFix" />
            <feColorMatrix
              in="SourceAlpha"
              type="matrix"
              values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
              result="hardAlpha"
            />
            <feOffset />
            <feGaussianBlur stdDeviation="3" />
            <feColorMatrix
              type="matrix"
              values="0 0 0 0 0.988235 0 0 0 0 0.454902 0 0 0 0 0 0 0 0 1 0"
            />
            <feBlend
              mode="normal"
              in2="BackgroundImageFix"
              result="effect1_dropShadow_0_94"
            />
            <feBlend
              mode="normal"
              in="SourceGraphic"
              in2="effect1_dropShadow_0_94"
              result="shape"
            />
          </filter>
        </defs>
      </svg>

      <h3>
        <span>{dayjs(date).local().format('MM/DD')}</span> {percent}%
      </h3>
    </CircleGraphStyled>
  );
};

CircleGraph.propTypes = {
  percent: PropTypes.number,
  date: PropTypes.any,
};

CircleGraph.defaultProps = {
  percent: 75,
  date: new Date(),
};

export default memo(CircleGraph);
