import React, { memo } from 'react';
import { Line } from '@ant-design/plots';

const LineGraph = ({ data }) => {
  const config = {
    data: data,
    xField: 'month',
    yField: 'value',
    seriesField: 'category',
    colorField: 'category', // or seriesField in some cases
    height: 268,
    color: ({ category }) => {
      if(category === 'yellow'){
        return '#FFCC21';
      }
      return '#8FE9D0';
    },
    lineStyle:{
      lineWidth: 3,
    },
    xAxis: {
      label: {
        style: {
          fill: 'white',
          fontSize: 12,
        },
        formatter: (name) => name,
      },
      title: {
        text: '',
      },
      line: {
        style: {
          stroke: '#777',
        },
      },
      grid: {
        line: {
          style: {
            stroke: '#777',
          },
        },
      },
    },
    yAxis: {
      label: {
        style: {
          fill: 'white',
          fontSize: 12,
        },
      },
      title: {
        text: '',
        style: {
          fontSize: 16,
        },
      },
      line: {
        style: {
          lineWidth: 0,
        },
      },
      tickLine: {
        style: {
          lineWidth: 3,
          stroke: '#777',
        },
        length: 5,
      },
      grid: {
        line: {
          style: {
            lineWidth: 0,
          },
        },
      },
    },

    point: {
      size: 8,
      style: {
        lineWidth: 0,
      },
    },
    legend: false,
  };

  return <Line {...config} />;
};

LineGraph.propTypes = {
  // percent: PropTypes.number,
};

LineGraph.defaultProps = {};

export default memo(LineGraph);
