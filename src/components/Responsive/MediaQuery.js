import { MEDIA_SIZE } from '@/utils/constants';
import { useMediaQuery } from 'react-responsive';

const Mobile = ({ children }) => {
  const isMobile = useMediaQuery({ maxWidth: MEDIA_SIZE.TABLET });
  return isMobile ? children : null;
};

const Tablet = ({ children }) => {
  const isTablet = useMediaQuery({
    minWidth: MEDIA_SIZE.TABLET,
    maxWidth: MEDIA_SIZE.DESKTOP,
  });
  return isTablet ? children : null;
};

const Desktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: MEDIA_SIZE.DESKTOP });
  return isDesktop ? children : null;
};

const NotDesktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: MEDIA_SIZE.DESKTOP });
  return !isDesktop ? children : null;
};

export { Mobile, Desktop, Tablet, NotDesktop };
