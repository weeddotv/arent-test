import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '@/styles/theme';
import { Button as ButtonAntd } from 'antd';

const ButtonStyled = styled(ButtonAntd)`
  font-size: 18rem;
  font-weight: 300;
  height: 56rem;
  color: white;
  border-radius: 4rem;
  border: 0;
  min-width: 296rem;
  max-width: 100%;
  font-family: 'Noto Sans JP', sans-serif;

  background-image: linear-gradient(
    33deg,
    ${colors.primary300} 0%,
    ${colors.primary400} 100%
  );
`;

const Button = ({ icon, children, ...rest }) => {
  return <ButtonStyled {...rest}>{children}</ButtonStyled>;
};
Button.propTypes = {
  className: PropTypes.string,
};

Button.defaultProps = {
  className: '',
};

export default memo(Button);
