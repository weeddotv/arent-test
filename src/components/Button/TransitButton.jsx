import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Image from 'next/image';
import styled from 'styled-components';
import colors from '@/styles/theme';

const ButtonStyled = styled.button`
  position: relative;
  width: 116rem;
  height: 134rem;
  clip-path: polygon(50% 0, 100% 25%, 100% 75%, 50% 100%, 0 75%, 0 25%);
  padding: 0;
  border: 0;
  transition: 0.3s ease-in-out;
  background-image: linear-gradient(
    66deg,
    ${colors.primary300} 0%,
    ${colors.primary400} 100%
  );
  img {
    width: 53rem;
    margin: 0 auto 5rem;
  }
  span {
    display: block;
    font-size: 20rem;
    line-height: 24rem;
    color: white;
  }
  &:hover {
    transform: scale(1.04);
    transition: 0.3s ease-in-out;
  }
`;

const TransitButton = ({ icon, children, ...rest }) => {
  return (
    <ButtonStyled {...rest}>
      <Image width={53} height={45} src={icon} alt="icon" />
      <span>{children}</span>
    </ButtonStyled>
  );
};
TransitButton.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.string,
};

TransitButton.defaultProps = {
  className: '',
  icon: '/images/icon_knife.svg',
};

export default memo(TransitButton);
