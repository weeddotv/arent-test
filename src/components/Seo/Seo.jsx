import Head from 'next/head';
import React, { memo } from 'react';

const Seo = (props) => {
  if (!props) return null;

  return (
    <Head>
      <title>{props?.title || 'Arent - test'}</title>
      {props?.title && <meta name="title" content={props?.title} />}
      {props?.description && (
        <meta property="description" content={props?.description} />
      )}
      {props?.url && <meta property="og:url" content={props?.url} />}
      {props?.description && (
        <meta
          property="og:description"
          content={props?.description}
          key="ogdesc"
        />
      )}
      {props?.image && (
        <meta property="og:image" content={props?.image} key="ogimage" />
      )}
      {props?.keywords && <meta name="keywords" content={props?.keywords} />}
    </Head>
  );
};

export default memo(Seo);
