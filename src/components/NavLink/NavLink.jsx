import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '@/styles/theme';

const NavLinkStyled = styled(Link)`
  &.isActive {
    color: ${colors.primary400};
  }
`;

const NavLink = ({ href, exact, children, className, ...rest }) => {
  const { pathname } = useRouter();
  const isActive = exact ? pathname === href : pathname.startsWith(href);

  return (
    <NavLinkStyled
      href={href}
      className={`${className} ${isActive ? 'isActive' : ''}`}
      {...rest}
    >
      {children}
    </NavLinkStyled>
  );
};
NavLink.propTypes = {
  href: PropTypes.string,
  exact: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.any,
};

NavLink.defaultProps = {
  href: '/',
  exact: false,
  className: '',
  children: null,
};

export default memo(NavLink);
