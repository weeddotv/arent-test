import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '@/styles/theme';
import dayjs from 'dayjs';

const DiaryItemStyled = styled.div`
  padding: 16rem 16rem 47rem;
  color: ${colors.dark500};
  border: 2rem solid #707070;
  .ar-date {
    margin-bottom: 8rem;
    h3 {
      font-size: 18rem;
      font-weight: 400;
      line-height: 22rem;
      letter-spacing: 0.09px;
      max-width: 140rem;
    }
  }
  .ar-desc {
    p {
      font-size: 12rem;
      font-weight: 300;
      line-height: 17rem;
      letter-spacing: 0.06px;
      font-family: 'Noto Sans JP', sans-serif;
      overflow: hidden;
      display: -webkit-box;
      -webkit-line-clamp: 7; /* number of lines to show */
      line-clamp: 7;
      -webkit-box-orient: vertical;
      height: 102rem;
    }
  }
`;

const DiaryItem = ({ time, desc, ...rest }) => {
  return (
    <DiaryItemStyled {...rest}>
      <div className="ar-date">
        <h3>{dayjs(time).local().format('YYYY.MM.DD HH.mm')}</h3>
      </div>
      <div className="ar-desc">
        <p>{desc}</p>
      </div>
    </DiaryItemStyled>
  );
};
DiaryItem.propTypes = {
  time: PropTypes.string,
  desc: PropTypes.string,
};

DiaryItem.defaultProps = {
  time: '2021.05.21 23:25',
  desc: '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
};

export default memo(DiaryItem);
