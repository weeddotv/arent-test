import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '@/styles/theme';

const RecordStyled = styled.div`
  border: solid 24rem ${colors.primary300};
  height: 288rem;
  width: 288rem;
  position: relative;
  background-color: rgba(0, 0, 0, 0.75);
  background-image: url(${(props) => props.imageUrl});
  background-blend-mode: luminosity;
  background-size: cover;
  margin: 0 auto;
  .ar-content {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    width: calc(100% - 48rem);
    text-align: center;
    h3 {
      font-size: 25rem;
      font-weight: 400;
      line-height: 30rem;
      letter-spacing: 0.125px;
      color: ${colors.primary300};
      margin-bottom: 11rem;
    }
    p {
      font-size: 14rem;
      font-weight: 300;
      line-height: 20rem;
      padding: 1px 0 3rem;
      min-width: 160rem;
      background-color: ${colors.primary400};
      color: white;
      display: inline-block;
      font-family: 'Noto Sans JP', sans-serif;
    }
  }
  &:hover {
    background-blend-mode: initial;
    transition: 0.3s ease-in-out;
    transform: scale(0.98);
  }
`;

const Record = ({ imageUrl, title, desc, ...rest }) => {
  return (
    <RecordStyled imageUrl={imageUrl} {...rest}>
      <div className="ar-content">
        <h3>{title}</h3>
        <p>{desc}</p>
      </div>
    </RecordStyled>
  );
};
Record.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  imageUrl: PropTypes.string,
};

Record.defaultProps = {
  title: 'BODY RECORD',
  desc: '自分のカラダの記録',
  imageUrl: '/images/d01.jpg',
};

export default memo(Record);
