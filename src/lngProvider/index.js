import enLang from './entries/en-US';
import koLang from './entries/ko_KR';
import jaLang from './entries/ja_JP';
import zhLang from './entries/zh_CN';
import esLang from './entries/es_ES';
import mnLang from './entries/mn_MN';
import idLang from './entries/id_ID';
import viLang from './entries/vi_VN';

const AppLocale = {
  en: enLang,
  ko: koLang,
  ja: jaLang,
  zh: zhLang,
  es: esLang,
  mn: mnLang,
  id: idLang,
  vi: viLang,
};

export default AppLocale;
