import esMessages from '../locales/es_ES.json';
import antdES from 'antd/locale/es_ES';

const EsLang = {
  messages: {
    ...esMessages,
  },
  antd: antdES,
  locale: 'es-ES',
};
export default EsLang;
