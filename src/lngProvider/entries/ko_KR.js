import koMessages from '../locales/ko_KR.json';
import antdKo from 'antd/locale/ko_KR';

const KrLang = {
  messages: {
    ...koMessages,
  },
  antd: antdKo,
  locale: 'ko-KR',
};
export default KrLang;
