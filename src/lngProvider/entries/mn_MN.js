import MnMessages from '../locales/mn_MN.json';
import antMN from 'antd/locale/mn_MN';

const MnLang = {
  messages: {
    ...MnMessages,
  },
  antd: antMN,
  locale: 'mn-MN',
};
export default MnLang;
