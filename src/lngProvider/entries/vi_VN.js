import viMessages from '../locales/vi_VN.json';
import antVN from 'antd/locale/vi_VN';

const ViLang = {
  messages: {
    ...viMessages,
  },
  antd: antVN,
  locale: 'vi-VN',
};
export default ViLang;
