import idMessages from '../locales/id_ID.json';
import antdID from 'antd/locale/id_ID';

const IdLang = {
  messages: {
    ...idMessages,
  },
  antd: antdID,
  locale: 'id-ID',
};
export default IdLang;
