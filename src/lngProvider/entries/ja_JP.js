import jaMessages from '../locales/ja_JP.json';
import antdJP from 'antd/locale/ja_JP';

const JaLang = {
  messages: {
    ...jaMessages,
  },
  antd: antdJP,
  locale: 'ja-JP',
};
export default JaLang;
