import zhMessages from '../locales/zh_CN.json';
import antCN from 'antd/locale/zh_CN';

const ZhLang = {
  messages: {
    ...zhMessages,
  },
  antd: antCN,
  locale: 'zh-CN',
};
export default ZhLang;
