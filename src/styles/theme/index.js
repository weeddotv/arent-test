export const colors = {
  light: 'white',
  primary300: '#FFCC21',
  primary400: '#FF963C',
  primary500: '#EA6C00',
  secondary300: '#8FE9D0',
  dark600: '#2E2E2E',
  dark500: '#414141',
  gray400: '#777',
  
};

export default colors;
