import { Desktop, Mobile, NavLink, NotDesktop } from '@/components';
import { useOnClickOutside } from '@/hooks';
import colors from '@/styles/theme';
import { ROUTE } from '@/utils/constants';
import { Button } from 'antd';
import { signOut, useSession } from 'next-auth/react';
import Image from 'next/image';
import Link from 'next/link';
import React, { memo, useRef, useState } from 'react';
import styled from 'styled-components';

const HeaderStyled = styled.header`
  background-color: ${colors.dark500};
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
`;
const Container = styled.div`
  max-width: 992rem;
  margin: 0 auto;
  padding: 0 16rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Left = styled.div`
  a {
    display: block;
    padding: 16rem 19rem 8rem 16rem;
    img {
      width: 109rem;
      display: block;
    }
  }
`;

const Right = styled.div`
  display: flex;
  align-items: center;
  .ar-b-menu {
    display: flex;
    align-items: center;
    li {
      padding: 16rem 0;
      margin-right: 16rem;
      padding-right: 16rem;
      a {
        display: flex;
        align-items: center;
        margin-right: 24rem;
        font-family: 'Noto Sans JP', sans-serif;
        font-size: 16rem;
        font-weight: 300;
        line-height: 23rem;
        transition: 0.3s ease-in-out;
        .ar-icon {
          position: relative;
          margin-right: 8rem;
          img {
            width: 32rem;
          }
          .ar-amount {
            position: absolute;
            right: 0;
            top: 0;
            width: 16rem;
            height: 16rem;
            background-color: ${colors.primary500};
            border-radius: 50%;
            transform: translate(50%, 0);
            font-size: 10rem;
            font-weight: 400;
            line-height: 16rem;
            text-align: center;
            display: inline-block;
          }
        }

        &:last-child {
          margin-right: 0;
        }
        &:hover {
          color: ${colors.primary400};
          transition: 0.3s ease-in-out;
        }
      }
    }
  }
`;

const NavStyled = styled.nav`
  position: relative;
  .ar-btn {
    display: block;
    img {
      width: 32rem;
    }
  }
  ul {
    position: absolute;
    z-index: -1;
    opacity: 0;
    bottom: 0;
    right: 0;
    visibility: hidden;
    background-color: ${colors.gray400};
    transform: translate(10rem, 100%) scale(0.96);
    min-width: 280rem;
    transition: 0.2s ease-in-out;

    li {
      border-top: 1px solid rgba(255, 255, 255, 0.15);
      border-bottom: 1px solid rgba(46, 46, 46, 0.25);
      a,
      button {
        display: block;
        width: 100%;
        font-size: 18rem;
        font-weight: 300;
        line-height: 26rem;
        padding: 23rem 0 23rem 32rem;
        color: white;
        font-family: 'Noto Sans JP', sans-serif;
        transition: 0.3s ease-in-out;
        text-align: left;

        &:hover {
          color: ${colors.primary400};
          transition: 0.3s ease-in-out;
        }
      }
    }
  }
  &.isActive {
    ul {
      z-index: 13;
      opacity: 1;
      transform: translate(0, 100%) scale(1);
      transition: 0.2s ease-in-out;
      visibility: inherit;
    }
  }
`;

const Header = () => {
  const ref = useRef();
  const [activeNav, setActiveNav] = useState(false);
  const { data: session } = useSession();

  useOnClickOutside(ref, () => {
    setActiveNav(false);
  });

  const toggletNav = () => {
    setActiveNav(!activeNav);
  };
  const onSignOut = async () => {
    signOut();
  };

  return (
    <HeaderStyled>
      <Container>
        <Left>
          <Link href={ROUTE.TOP}>
            <Image width={109} height={40} src="/images/logo.svg" alt="logo" />
          </Link>
        </Left>
        <Right>
          <Desktop>
            {!!session && (
              <ul className="ar-b-menu">
                <li>
                  <NavLink href={ROUTE.MY_RECORD}>
                    <div className="ar-icon">
                      <Image
                        width={32}
                        height={32}
                        src="/images/icon_memo.svg"
                        alt="icon"
                      />
                    </div>
                    自分の記録
                  </NavLink>
                </li>
                <li>
                  <NavLink href="#">
                    <div className="ar-icon">
                      <Image
                        width={32}
                        height={32}
                        src="/images/icon_challenge.svg"
                        alt="icon"
                      />
                    </div>
                    チャレンジ
                  </NavLink>
                </li>
                <li>
                  <NavLink href="#">
                    <div className="ar-icon">
                      <span className="ar-amount">1</span>
                      <Image
                        width={32}
                        height={32}
                        src="/images/icon_info.svg"
                        alt="icon"
                      />
                    </div>
                    お知らせ
                  </NavLink>
                </li>
              </ul>
            )}
          </Desktop>

          <NavStyled className={activeNav ? 'isActive' : ''} ref={ref}>
            <Button className="ar-btn noStyle" onClick={toggletNav}>
              <Image
                width={32}
                height={32}
                src={
                  !activeNav
                    ? '/images/icon_menu.svg'
                    : '/images/icon_close.svg'
                }
                alt="icon"
              />
            </Button>

            <ul>
              <li>
                <NavLink href={ROUTE.MY_RECORD}>自分の記録</NavLink>
              </li>
              <li>
                <NavLink href="#">体重グラフ</NavLink>
              </li>
              <li>
                <NavLink href="#">目標</NavLink>
              </li>
              <li>
                <NavLink href="#">選択中のコース</NavLink>
              </li>
              <li>
                <NavLink href={ROUTE.COLUMN}>コラム一覧</NavLink>
              </li>
              <li>
                <NavLink href="#">設定</NavLink>
              </li>
              <NotDesktop>
                <>
                  <li>
                    <NavLink href="#">チャレンジ</NavLink>
                  </li>

                  <li>
                    <NavLink href="#">お知らせ</NavLink>
                  </li>
                </>
              </NotDesktop>

              {!!session && (
                <li>
                  <button onClick={onSignOut} className="noStyle">
                    Sign Out
                  </button>
                </li>
              )}
            </ul>
          </NavStyled>
        </Right>
      </Container>
    </HeaderStyled>
  );
};

export default memo(Header);
