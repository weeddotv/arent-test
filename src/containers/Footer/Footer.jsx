import { NavLink } from '@/components';
import colors from '@/styles/theme';
import { MEDIA_SIZE, ROUTE } from '@/utils/constants';
import React, { memo } from 'react';
import styled from 'styled-components';

const FooterStyled = styled.footer`
  background-color: ${colors.dark600};
  padding: 56rem 0;
  margin-top: auto;
`;

const Container = styled.div`
  max-width: 992rem;
  margin: 0 auto;
  padding: 0 16rem;
  ul {
    display: flex;
    li {
      margin-right: 45rem;
      display: block;
      a {
        font-size: 11rem;
        font-weight: 300;
        line-height: 16rem;
        letter-spacing: 0.033px;
        display: block;
        font-family: 'Noto Sans JP', sans-serif;
        &:hover {
          text-decoration: underline;
        }
      }
      &:last-child {
        margin-right: 0;
      }
    }
  }
  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    ul {
      display: block;
      li {
        margin-bottom: 8rem;
        margin-right: 24rem;
        display: inline-block;
        &:last-child {
          margin-bottom: 0;
        }
      }
    }
  }
`;

const Footer = () => {
  return (
    <FooterStyled>
      <Container>
        <ul>
          <li>
            <NavLink href="#">会員登録</NavLink>
          </li>
          <li>
            <NavLink href="#">運営会社</NavLink>
          </li>
          <li>
            <NavLink href="#">利用規約</NavLink>
          </li>
          <li>
            <NavLink href="#">個人情報の取扱について</NavLink>
          </li>
          <li>
            <NavLink href="#">特定商取引法に基づく表記</NavLink>
          </li>
          <li>
            <NavLink href="#">お問い合わせ</NavLink>
          </li>
        </ul>
      </Container>
    </FooterStyled>
  );
};

export default memo(Footer);
