import styled from 'styled-components';
import { DefaultLayout } from '@/layouts';
import { memo, useEffect } from 'react';
import { Button, DiaryItem, LineGraph, Record } from '@/components';
import colors from '@/styles/theme';
import { MEDIA_SIZE } from '@/utils/constants';
import { useDispatch, useSelector } from '@/hooks';
import { isEmpty } from 'lodash';
import dayjs from 'dayjs';
import { Link as LinkScroll, Element } from 'react-scroll';

const MyRecordPageStyled = styled.section`
  padding: 56rem 0 64rem;
`;
const Container = styled.div`
  max-width: 992rem;
  margin: 0 auto;
  padding: 0 16rem;
`;

const BodyRecord = styled.div`
  margin-bottom: 56rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  a {
    cursor: pointer;
  }
  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    display: block;
    text-align: center;
    button {
      width: 100%;
    }
  }
`;
const ChartStyled = styled.div`
  background-color: ${colors.dark500};
  margin-bottom: 56rem;
  padding: 16rem 24rem 18rem;
  ul {
    display: flex;
    padding: 0;
    margin: 0;
    li {
      margin-right: 16rem;
      list-style: none;
      button {
        height: 26rem;
        border-radius: 11rem;
        line-height: 26rem;
        font-size: 15rem;
        font-weight: 300;
        background-color: white;
        color: ${colors.primary300};
        transition: 0.3s ease-in-out;
        padding: 0 20rem;
        font-family: 'Noto Sans JP', sans-serif;

        &:hover {
          background-color: ${colors.primary300};
          color: white;
          transition: 0.3s ease-in-out;
        }
      }
      &:last-child {
        margin-right: 0;
      }
    }
  }
`;
const MyExercise = styled.div`
  background-color: ${colors.dark500};
  margin-bottom: 56rem;
  padding: 16rem 24rem 24rem;
  .ar-b-head {
    display: flex;
    margin-bottom: 4rem;
    h2 {
      font-size: 15rem;
      font-weight: 400;
      line-height: 18rem;
      letter-spacing: 0.15px;
      max-width: 96rem;
    }
    h3 {
      font-size: 22rem;
      font-weight: 400;
      line-height: 27rem;
      letter-spacing: 0.11px;
    }
  }
  .ar-b-body {
    ul {
      display: flex;
      flex-wrap: wrap;
      padding-left: 0;
      justify-content: space-between;
      padding-right: 30rem;
      max-height: 192rem;
      overflow: auto;
      li {
        width: calc(50% - 20rem);
        display: flex;
        justify-content: space-between;
        position: relative;
        padding-left: 16rem;
        border-bottom: 1px solid #777;
        padding-bottom: 2rem;
        margin-bottom: 8rem;
        &::before {
          content: '';
          position: absolute;
          width: 4rem;
          height: 4rem;
          background-color: white;
          left: 0;
          top: 11rem;
          border-radius: 50%;
          transform: translate(0, -50%);
        }
        .ar-b-left {
          margin-right: 5rem;
          width: calc(100% - 75rem);
          h4 {
            font-size: 15rem;
            font-weight: 300;
            line-height: 22rem;
            letter-spacing: 0.075px;
            font-family: 'Noto Sans JP', sans-serif;
            overflow: hidden;
            display: -webkit-box;
            -webkit-line-clamp: 1; /* number of lines to show */
            line-clamp: 1;
            -webkit-box-orient: vertical;
          }
          p {
            font-size: 15rem;
            font-weight: 400;
            line-height: 18rem;
            letter-spacing: 0.075px;
            color: ${colors.primary300};
          }
        }
        .ar-b-right {
          p {
            font-size: 18rem;
            font-weight: 400;
            line-height: 22rem;
            letter-spacing: 0.09px;
            color: ${colors.primary300};
          }
        }
      }
    }
  }

  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    .ar-b-body {
      ul {
        display: block;
        max-height: 384rem;
        li {
          width: 100%;
          margin-bottom: 28rem;
        }
      }
    }
  }
`;

const MyDiary = styled.div`
  h2 {
    font-size: 22rem;
    font-weight: 400;
    line-height: 27rem;
    letter-spacing: 0.11px;
    color: ${colors.dark500};
  }
  .ar-b-items {
    display: flex;
    flex-wrap: wrap;
    gap: 12rem;
    margin-bottom: 28rem;
    .ar-item {
      width: 231rem;
    }
  }
  .ar-b-loadmore {
    text-align: center;
  }

  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    .ar-b-items {
      display: block;
      .ar-item {
        width: 100%;
        margin-bottom: 28rem;
      }
    }
  }
`;

function MyRecord() {
  const bodyWeight = useSelector(({ UserStore }) => UserStore.bodyWeight);
  const myExercise = useSelector(({ UserStore }) => UserStore.myExercise);
  const myDiary = useSelector(({ UserStore }) => UserStore.myDiary);

  const { getBodyWeight, getMyExercise, getMyDiary, loadMoreMyDiary } =
    useDispatch(({ UserStore }) => ({
      getBodyWeight: UserStore.getBodyWeight,
      getMyExercise: UserStore.getMyExercise,
      getMyDiary: UserStore.getMyDiary,
      loadMoreMyDiary: UserStore.loadMoreMyDiary,
    }));

  useEffect(() => {
    getBodyWeight();
    getMyExercise();
    getMyDiary({
      pagination: {
        pageSize: 8,
        current: 1,
      },
    });
  }, []);

  const onLoadMore = () => {
    loadMoreMyDiary({
      pagination: {
        pageSize: 8,
        current: +myDiary.pagination.current + 1,
      },
    });
  };

  return (
    <DefaultLayout isPrivate={true}>
      <MyRecordPageStyled>
        <Container>
          <BodyRecord>
            <LinkScroll
              to="bodyRerord"
              smooth={true}
              offset={50}
              duration={500}
            >
              <Record
                title="BODY RECORD"
                desc="自分のカラダの記録"
                imageUrl="/images/MyRecommend-1.png"
              />
            </LinkScroll>
            <LinkScroll
              to="myExercise"
              smooth={true}
              offset={50}
              duration={500}
            >
              <Record
                title="MY EXERCISE"
                desc="自分の運動の記録"
                imageUrl="/images/MyRecommend-2.png"
              />
            </LinkScroll>
            <LinkScroll to="myDiary" smooth={true} offset={50} duration={500}>
              <Record
                title="MY DIARY"
                desc="自分の日記"
                imageUrl="/images/MyRecommend-3.png"
              />
            </LinkScroll>
          </BodyRecord>
          <Element name="bodyRerord">
            {!isEmpty(bodyWeight) && (
              <ChartStyled>
                <LineGraph data={bodyWeight} />
                <ul>
                  <li>
                    <button className="noStyle">日</button>
                  </li>
                  <li>
                    <button className="noStyle">週</button>
                  </li>
                  <li>
                    <button className="noStyle">月</button>
                  </li>
                  <li>
                    <button className="noStyle">年</button>
                  </li>
                </ul>
              </ChartStyled>
            )}
          </Element>

          <Element name="myExercise">
            <MyExercise>
              <div className="ar-b-head">
                <h2>MY EXERCISE</h2>
                <h3>{dayjs().local().format('YYYY.MM.DD')}</h3>
              </div>
              <div className="ar-b-body">
                <ul className="ar-scrollBar">
                  {myExercise.items.map((item) => (
                    <li key={item.id}>
                      <div className="ar-b-left">
                        <h4>{item.title}</h4>
                        <p>{item.kcal}kcal</p>
                      </div>
                      <div className="ar-b-right">
                        <p>{item.duration} min</p>
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            </MyExercise>
          </Element>

          <Element name="myDiary">
            <MyDiary>
              <h2>MY DIARY</h2>
              <div className="ar-b-items">
                {myDiary.items.map((item) => (
                  <DiaryItem
                    key={item.id}
                    className="ar-item"
                    time={item.date}
                    desc={item.description}
                  />
                ))}
              </div>
              {myDiary.items.length < myDiary.pagination.total && (
                <div className="ar-b-loadmore">
                  <Button onClick={onLoadMore}>記録をもっと見る</Button>
                </div>
              )}
            </MyDiary>
          </Element>
        </Container>
      </MyRecordPageStyled>
    </DefaultLayout>
  );
}

export default memo(MyRecord);
