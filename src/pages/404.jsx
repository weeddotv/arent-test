import { memo } from 'react';
import styled from 'styled-components';

const ErrorPageStyled = styled.section`
`;

const Container = styled.div`
`;
const Head = styled.div`

`;
const Body = styled.div`

`;
const Foot = styled.div`

`;

function Custom404() {
  return (
    <ErrorPageStyled>
      <Container>
        <Head>
        </Head>
        <Body>
          
        </Body>
        <Foot>
         
        </Foot>
      </Container>
    </ErrorPageStyled>
  );
}
export default memo(Custom404);
