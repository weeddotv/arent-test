import '../styles/global.scss';
import { useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import { ConfigProvider } from 'antd';
import store from '@/stores';
import colors from '@/styles/theme';
import { SessionProvider } from 'next-auth/react';

import dayjs from 'dayjs';
import dayjsPluginUTC from 'dayjs/plugin/utc';
dayjs.extend(dayjsPluginUTC);

function MyApp({ Component, pageProps: { session, ...pageProps } }) {
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    setIsLoading(false);
  }, []);

  if (isLoading) return null; // prevent the break image/css

  return (
    <Provider store={store}>
      <ConfigProvider
        theme={{
          token: {
            colorPrimary: colors.primary300,
          },
        }}
      >
        <SessionProvider session={session}>
          <Component {...pageProps} />
        </SessionProvider>
      </ConfigProvider>
    </Provider>
  );
}

export default MyApp;
