import colors from '@/styles/theme';
import { Html, Head, Main, NextScript } from 'next/document';

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <meta name="referrer" content="always" />
        <meta property="description" content="description" />
        <meta property="og:description" content={'description'} key="ogdesc" />
        <meta property="og:url" content={'https://arent.co.jp/'} />
        <meta property="og:image" content={'/images/cover.jpg'} key="ogimage" />
        <meta name="keywords" content={'Arent test'} />
        <meta property="og:site_name" content="Arent test" />

        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;700&family=Noto+Sans+JP:wght@300;400;700;800;900&display=swap"
          rel="stylesheet"
        />

        <meta name="theme-color" content={colors.primary} />
        <link rel="shortcut icon" href="/favicon.png" />
        <link
          rel="apple-touch-icon"
          sizes="60x60"
          href="/favicon60x60.png"
        />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
