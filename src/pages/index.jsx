import styled from 'styled-components';
import { DefaultLayout } from '@/layouts';
import { memo, useEffect, useState } from 'react';
import { Button, CircleGraph, LineGraph, TransitButton } from '@/components';
import colors from '@/styles/theme';
import Link from 'next/link';
import { MEAL_HISTORY_TYPE, MEDIA_SIZE } from '@/utils/constants';
import { useSelector, useDispatch } from '@/hooks';
import dayjs from 'dayjs';
import { isEmpty } from 'lodash';

const Container = styled.div`
  max-width: 992rem;
  margin: 0 auto;
  padding: 0 16rem;
`;

const Head = styled.section`
  display: flex;
  align-items: center;
  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    display: block;
  }
`;
const Achievement = styled.div`
  width: 42%;
  background: url('/images/d01.jpg') center no-repeat;
  background-size: cover;
  padding: 66rem 0 68rem;
  .ar-b-circle {
    position: relative;
    padding: 66rem 0 68rem;
    display: flex;
    align-items: center;
    justify-content: center;
    .ar-circle {
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      width: 181rem;
      height: 181rem;
    }
  }
  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    width: 100%;
  }
`;
const BodyWeight = styled.div`
  width: 58%;
  height: 315rem;
  background-color: ${colors.dark600};
  overflow: hidden;
  padding: 12rem 32rem 24rem;
  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    width: 100%;
  }
`;

const Body = styled.section`
  padding: 24rem 0;
  ul {
    display: flex;
    align-items: center;
    justify-content: space-between;
    gap: 84rem;
    max-width: 788rem;
    margin: 0 auto;
  }
  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    ul {
      display: block;
      li {
        display: inline-block;
        width: 50%;
        text-align: center;
        margin-bottom: 24rem;
      }
    }
  }
`;
const Foot = styled.section`
  padding: 0 0 64rem;
  ul {
    display: flex;
    flex-wrap: wrap;
    gap: 8rem;
    margin-bottom: 28rem;
    li {
      width: calc(25% - 6rem);
      a {
        display: block;
        height: 234rem;
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        position: relative;
        overflow: hidden;
        transition: 0.3s ease-in-out;
        p {
          font-size: 15rem;
          line-height: 18rem;
          letter-spacing: 0.15px;
          position: absolute;
          bottom: 0;
          left: 0;
          color: white;
          padding: 7rem;
          background-color: ${colors.primary300};
        }
        &:hover {
          transform: translateY(-5rem);
          transition: 0.3s ease-in-out;
        }
      }
    }
  }
  .ar-b-loadmore {
    text-align: center;
  }
  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    ul {
      display: block;
      li {
        width: 100%;
        margin-bottom: 8rem;
      }
    }
  }
`;

function Home() {
  const [currentMealType, setCurrentMealType] = useState(undefined);
  const achievement = useSelector(({ UserStore }) => UserStore.achievement);
  const bodyWeight = useSelector(({ UserStore }) => UserStore.bodyWeight);
  const mealHistory = useSelector(({ UserStore }) => UserStore.mealHistory);

  const { getAchievement, getMealHistory, loadMoreMealHistory, getBodyWeight } =
    useDispatch(({ UserStore }) => ({
      getAchievement: UserStore.getAchievement,
      getMealHistory: UserStore.getMealHistory,
      loadMoreMealHistory: UserStore.loadMoreMealHistory,
      getBodyWeight: UserStore.getBodyWeight,
    }));

  useEffect(() => {
    getAchievement();
    getMealHistory({
      pagination: {
        pageSize: 8,
        current: 1,
      },
    });
    getBodyWeight();
  }, []);

  const onLoadMore = () => {
    loadMoreMealHistory({
      pagination: {
        pageSize: 8,
        current: +mealHistory.pagination.current + 1,
      },
      filters: {
        type: currentMealType,
      },
    });
  };

  const onFilter = (type) => {
    getMealHistory({
      pagination: {
        pageSize: 8,
        current: 1,
      },
      filters: {
        type: type,
      },
    });
    setCurrentMealType(type);
  };

  return (
    <DefaultLayout
      isPrivate={true}
      seo={{
        title: 'Arent testing',
        image: '/images/d01.jpg',
        description: 'Arent testing',
      }}
    >
      <Head>
        <Achievement>
          <CircleGraph
            className="ar-circle"
            percent={achievement.percent}
            date={achievement.date}
          />
        </Achievement>
        <BodyWeight>
          {!isEmpty(bodyWeight) && <LineGraph data={bodyWeight} />}
        </BodyWeight>
      </Head>
      <Body>
        <Container>
          <ul>
            <li>
              <TransitButton
                onClick={() => onFilter(MEAL_HISTORY_TYPE.MORNING)}
              >
                Morning
              </TransitButton>
            </li>
            <li>
              <TransitButton onClick={() => onFilter(MEAL_HISTORY_TYPE.LUNCH)}>
                Lunch
              </TransitButton>
            </li>
            <li>
              <TransitButton onClick={() => onFilter(MEAL_HISTORY_TYPE.DINER)}>
                Dinner
              </TransitButton>
            </li>
            <li>
              <TransitButton onClick={() => onFilter(MEAL_HISTORY_TYPE.SNACK)}>
                Snack
              </TransitButton>
            </li>
          </ul>
        </Container>
      </Body>

      <Foot>
        <Container>
          <ul>
            {mealHistory.items.map((item) => (
              <li key={item.id}>
                <Link
                  href="#"
                  style={{ backgroundImage: `url(${item.image})` }}
                >
                  <p>
                    {dayjs(item.date).local().format('MM.DD')}.{item.type}
                  </p>
                </Link>
              </li>
            ))}
          </ul>
          {mealHistory.items.length < mealHistory.pagination.total && (
            <div className="ar-b-loadmore">
              <Button onClick={onLoadMore}>記録をもっと見る</Button>
            </div>
          )}
        </Container>
      </Foot>
    </DefaultLayout>
  );
}

export default memo(Home);
