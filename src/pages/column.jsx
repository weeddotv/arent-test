import styled from 'styled-components';
import { DefaultLayout } from '@/layouts';
import { memo, useEffect } from 'react';
import { Button, ColumnHeadItem, ColumnItem } from '@/components';
import { MEDIA_SIZE } from '@/utils/constants';
import { useDispatch, useSelector } from '@/hooks';

const ColumnPageStyled = styled.section`
  padding: 56rem 0 64rem;
`;
const Container = styled.div`
  max-width: 992rem;
  margin: 0 auto;
  padding: 0 16rem;
`;

const Head = styled.div`
  margin-bottom: 56rem;
  display: flex;
  align-items: center;
  gap: 40rem;
  .ar-item {
    width: 216rem;
  }
  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    display: block;
    .ar-item {
      width: 100%;
      margin-bottom: 16rem;
    }
  }
`;

const Body = styled.div`
  .ar-b-items {
    display: flex;
    flex-wrap: wrap;
    margin-bottom: 26rem;
    gap: 8rem;
    .ar-item {
      width: calc(25% - 6rem);
      margin-bottom: 10rem;
    }
  }
  .ar-b-loadmore {
    text-align: center;
  }

  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    .ar-b-items {
      display: block;
      .ar-item {
        width: 100%;
      }
    }
  }
`;

function Column() {
  const columns = useSelector(({ AppStore }) => AppStore.columns);

  const { getColumns, loadMoreColumns } = useDispatch(({ AppStore }) => ({
    getColumns: AppStore.getColumns,
    loadMoreColumns: AppStore.loadMoreColumns,
  }));

  useEffect(() => {
    getColumns({
      pagination: {
        pageSize: 8,
        current: 1,
      },
    });
  }, []);

  const onLoadMore = () => {
    loadMoreColumns({
      pagination: {
        pageSize: 8,
        current: +columns.pagination.current + 1,
      },
    });
  };

  return (
    <DefaultLayout>
      <ColumnPageStyled>
        <Container>
          <Head>
            <ColumnHeadItem
              className="ar-item"
              title="RECOMMENDED COLUMN"
              desc="オススメ"
            />
            <ColumnHeadItem
              className="ar-item"
              title="RECOMMENDED DIET"
              desc="ダイエット"
            />
            <ColumnHeadItem
              className="ar-item"
              title="RECOMMENDED BEAUTY"
              desc="美容"
            />
            <ColumnHeadItem
              className="ar-item"
              title="RECOMMENDED HEALTH"
              desc="健康"
            />
          </Head>

          <Body>
            <div className="ar-b-items">
              {columns.items.map((item) => (
                <ColumnItem
                  key={item.id}
                  imageUrl={item.image}
                  title={item.title}
                  hashTags={item.type}
                  date={item.date}
                  url="#"
                  className="ar-item"
                />
              ))}
            </div>

            {columns.items.length < columns.pagination.total && (
              <div className="ar-b-loadmore">
                <Button onClick={onLoadMore}>記録をもっと見る</Button>
              </div>
            )}
          </Body>
        </Container>
      </ColumnPageStyled>
    </DefaultLayout>
  );
}

export default memo(Column);
