export default function handler(req, res) {
  switch (req.method) {
    case 'GET': {
      // TODO validate

      const data = [
        {
          id: 1,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/column-1.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
        {
          id: 2,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/column-2.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
        {
          id: 3,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/column-3.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
        {
          id: 4,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/column-4.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
        {
          id: 5,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/column-5.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
        {
          id: 6,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/column-6.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
        {
          id: 7,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/column-7.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
        {
          id: 8,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/column-8.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
        {
          id: 9,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/d01.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
        {
          id: 10,
          title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
          image: '/images/d01.jpg',
          date: new Date(),
          type: ['魚料理', '和食', 'DHA'],
        },
      ];
      const { type, pageSize, current } = req.query;

      let results = data;
      if (type) {
        results = results.filter((item) => item.type === type);
      }

      if (pageSize && current) {
        results = results.filter((item, index) => {
          if (current == 1) {
            if (index < +pageSize) {
              return item;
            }
          } else {
            if (
              index >= +pageSize * (+current - 1) &&
              index < +pageSize * +current
            ) {
              return item;
            }
          }
          return false;
        });
      }

      res.json({
        items: results,
        pagination: {
          pageSize,
          current,
          total: data.length,
        },
      });
    }
    default: {
      res.end();
    }
  }
}
