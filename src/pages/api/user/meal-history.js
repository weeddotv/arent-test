
export default function handler(req, res) {
  switch (req.method) {
    case 'GET': {
      // TODO validate

      const data = [
        {
          id: 1,
          type: 'Morning',
          image: '/images/m01.jpg',
          date: new Date(),
        },
        {
          id: 2,
          type: 'Morning',
          image: '/images/l03.jpg',
          date: new Date(),
        },
        {
          id: 3,
          type: 'Lunch',
          image: '/images/d01.jpg',
          date: new Date(),
        },
        {
          id: 4,
          type: 'Morning',
          image: '/images/l01.jpg',
          date: new Date(),
        },
        {
          id: 5,
          type: 'Snack',
          image: '/images/m01.jpg',
          date: new Date(),
        },
        {
          id: 6,
          type: 'Morning',
          image: '/images/l02.jpg',
          date: new Date(),
        },
        {
          id: 7,
          type: 'Lunch',
          image: '/images/s01.jpg',
          date: new Date(),
        },

        {
          id: 8,
          type: 'Morning',
          image: '/images/d01.jpg',
          date: new Date(),
        },
        {
          id: 9,
          type: 'Morning',
          image: '/images/m01.jpg',
          date: new Date(),
        },
        {
          id: 10,
          type: 'Lunch',
          image: '/images/d01.jpg',
          date: new Date(),
        },
        {
          id: 11,
          type: 'Morning',
          image: '/images/m01.jpg',
          date: new Date(),
        },
        {
          id: 12,
          type: 'Morning',
          image: '/images/d01.jpg',
          date: new Date(),
        },
        {
          id: 13,
          type: 'Morning',
          image: '/images/m01.jpg',
          date: new Date(),
        },
        {
          id: 14,
          type: 'Dinner',
          image: '/images/m01.jpg',
          date: new Date(),
        },
        {
          id: 15,
          type: 'Dinner',
          image: '/images/d01.jpg',
          date: new Date(),
        },
        {
          id: 16,
          type: 'Dinner',
          image: '/images/d01.jpg',
          date: new Date(),
        },
        {
          id: 17,
          type: 'Morning',
          image: '/images/m01.jpg',
          date: new Date(),
        },
        {
          id: 18,
          type: 'Dinner',
          image: '/images/m01.jpg',
          date: new Date(),
        },
        {
          id: 19,
          type: 'Dinner',
          image: '/images/d01.jpg',
          date: new Date(),
        },
        {
          id: 20,
          type: 'Dinner',
          image: '/images/d01.jpg',
          date: new Date(),
        },
      ];
      const { type, pageSize, current } = req.query;

      let results = data;
      if (type) {
        results = results.filter((item) => item.type === type);
      }

      if (pageSize && current) {
        results = results.filter((item, index) => {
          if (current == 1) {
            if (index < +pageSize) {
              return item;
            }
          } else {
            if (
              index >= +pageSize * (+current - 1) &&
              index < +pageSize * +current
            ) {
              return item;
            }
          }
          return false;
        });
      }

      res.json({
        items: results,
        pagination: {
          pageSize,
          current,
          total: !type ? data.length :  data.filter(item => item.type === type).length,
        },
      });
    }
    default: {
      res.end();
    }
  }
}
