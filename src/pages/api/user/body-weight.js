export default function handler(req, res) {
  switch (req.method) {
    case 'GET': {
      const results = [
        {
          month: '6月',
          value: 90,
          category: 'yellow',
        },
        {
          month: '6月',
          value: 90,
          category: 'green',
        },
        {
          month: '7月',
          value: 88,
          category: 'yellow',
        },
        {
          month: '7月',
          value: 89,
          category: 'green',
        },
        {
          month: '8月',
          value: 85,
          category: 'yellow',
        },
        {
          month: '月',
          value: 86,
          category: 'green',
        },
        {
          month: '9月',
          value: 85,
          category: 'yellow',
        },
        {
          month: '9月',
          value: 84,
          category: 'green',
        },
        {
          month: '10月',
          value: 80,
          category: 'yellow',
        },
        {
          month: '10月',
          value: 82,
          category: 'green',
        },
        {
          month: '11月',
          value: 79,
          category: 'yellow',
        },
        {
          month: '11月',
          value: 80,
          category: 'green',
        },
        {
          month: '12月',
          value: 78,
          category: 'yellow',
        },
        {
          month: '12月',
          value: 78,
          category: 'green',
        },
        {
          month: '1月',
          value: 77,
          category: 'yellow',
        },
        {
          month: '1月',
          value: 76,
          category: 'green',
        },
        {
          month: '2月',
          value: 67,
          category: 'yellow',
        },
        {
          month: '2月',
          value: 75,
          category: 'green',
        },
        {
          month: '3月',
          value: 58,
          category: 'yellow',
        },
        {
          month: '3月',
          value: 66,
          category: 'green',
        },
        {
          month: '4月',
          value: 76,
          category: 'yellow',
        },
        {
          month: '4月',
          value: 63,
          category: 'green',
        },
        {
          month: '5月',
          value: 74,
          category: 'yellow',
        },
        {
          month: '5月',
          value: 60,
          category: 'green',
        },
      ];

      res.json(results);
    }
    default: {
      res.end();
    }
  }
}
