export default function handler(req, res) {
  switch (req.method) {
    case 'GET': {
      res.json({ date: new Date(), percent: 99 });
    }
    default:
      res.end();
  }
}
