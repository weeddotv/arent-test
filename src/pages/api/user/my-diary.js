
export default function handler(req, res) {
  switch (req.method) {
    case 'GET': {
      // TODO validate

      const data = [
        {
          id: 1,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
        {
          id: 2,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
        {
          id: 3,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
        {
          id: 4,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
        {
          id: 5,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
        {
          id: 6,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
        {
          id: 7,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
        {
          id: 8,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
        {
          id: 9,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
        {
          id: 10,
          description:
            '私の日記の記録が一部表示されます。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…',
          date: new Date(),
        },
      ];
      const { type, pageSize, current } = req.query;

      let results = data;
      if (type) {
        results = results.filter((item) => item.type === type);
      }

      if (pageSize && current) {
        results = results.filter((item, index) => {
          if (current == 1) {
            if (index < +pageSize) {
              return item;
            }
          } else {
            if (
              index >= +pageSize * (+current - 1) &&
              index < +pageSize * +current
            ) {
              return item;
            }
          }
          return false;
        });
      }

      res.json({
        items: results,
        pagination: {
          pageSize,
          current,
          total: data.length,
        },
      });
    }
    default: {
      res.end();
    }
  }
}
