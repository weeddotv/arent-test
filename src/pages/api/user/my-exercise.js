export default function handler(req, res) {
  switch (req.method) {
    case 'GET': {
      // TODO validate

      const data = [
        {
          id: 1,
          title: '家事全般（立位・軽い)',
          kcal: 26,
          duration: 10,
        },
        {
          id: 2,
          title: '家事全般（立位・軽い)',
          kcal: 40,
          duration: 20,
        },
        {
          id: 3,
          title: '家事全般（立位・軽い)',
          kcal: 26,
          duration: 10,
        },
        {
          id: 4,
          title: '家事全般（立位・軽い)',
          kcal: 26,
          duration: 10,
        },
        {
          id: 5,
          title: '家事全般（立位・軽い)',
          kcal: 40,
          duration: 40,
        },
        {
          id: 6,
          title: '家事全般（立位・軽い)',
          kcal: 26,
          duration: 10,
        },
        {
          id: 7,
          title: '家事全般（立位・軽い)',
          kcal: 26,
          duration: 20,
        },
        {
          id: 8,
          title: '家事全般（立位・軽い)',
          kcal: 40,
          duration: 10,
        },
        {
          id: 9,
          title: '家事全般（立位・軽い)',
          kcal: 26,
          duration: 30,
        },
        {
          id: 10,
          title: '家事全般（立位・軽い)',
          kcal: 26,
          duration: 20,
        },
      ];
      const { type, pageSize, current } = req.query;

      let results = data;
      if (type) {
        results = results.filter((item) => item.type === type);
      }

      if (pageSize && current) {
        results = results.filter((item, index) => {
          if (current == 1) {
            if (index < +pageSize) {
              return item;
            }
          } else {
            if (
              index >= +pageSize * (+current - 1) &&
              index < +pageSize * +current
            ) {
              return item;
            }
          }
          return false;
        });
      }

      res.json({
        items: results,
        pagination: {
          pageSize,
          current,
          total: data.length,
        },
      });
    }
    default: {
      res.end();
    }
  }
}
