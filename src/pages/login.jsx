import styled from 'styled-components';
import { DefaultLayout } from '@/layouts';
import { memo } from 'react';
import { signIn } from 'next-auth/react';
import { Button } from '@/components';

const LoginPageStyled = styled.section`
  padding: 80rem 0;
`;
const Container = styled.div`
  max-width: 992rem;
  margin: 0 auto;
  padding: 0 16rem;
  text-align: center;
  .ar-b-login {
    padding: 120rem 24rem;
    max-width: 440rem;
    margin: 0 auto;
    border-radius: 16rem;
    border: 1px solid rgba(0, 0, 0, 0.2);
    button {
      min-width: 200rem;
    }
  }
`;

function Home() {
  return (
    <DefaultLayout>
      <LoginPageStyled>
        <Container>
          <div className="ar-b-login">
            <Button
              onClick={() =>
                signIn('google', {
                  callbackUrl: process.env.NEXT_PUBLIC_PUBLIC_URL,
                })
              }
            >
              Sign in
            </Button>
          </div>
        </Container>
      </LoginPageStyled>
    </DefaultLayout>
  );
}

export default memo(Home);
