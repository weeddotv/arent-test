import React, { memo, useEffect, useState } from 'react';
import styled from 'styled-components';
import { Seo } from '@/components';
import { Footer, Header } from '@/containers';
import { MEDIA_SIZE, ROUTE } from '@/utils/constants';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import Image from 'next/image';
import { animateScroll } from 'react-scroll';

const DefaultLayoutStyled = styled.main`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  position: relative;
  .ar-btn-scrollToTop {
    position: fixed;
    bottom: 272rem;
    right: 96rem;
    background-color: white;
    border-radius: 50%;
    img {
      width: 48rem;
    }
  }
  @media (max-width: ${MEDIA_SIZE.TABLET}px) {
    .ar-btn-scrollToTop {
      bottom: 16rem;
      right: 16rem;
    }
  }
`;

const DefaultLayout = ({ seo, isPrivate, children }) => {
  const router = useRouter();
  const [showScrollToTop, setShowScrollToTop] = useState(false);
  const { data: session, status } = useSession();
  if (isPrivate && status !== 'loading' && !session) {
    router.push(ROUTE.LOGIN);
  }

  useEffect(() => {
    document.addEventListener('scroll', (_event) => {
      const lastKnownScrollPosition = window.scrollY;
      if (lastKnownScrollPosition > 100) {
        setShowScrollToTop(true);
      } else {
        setShowScrollToTop(false);
      }
    });
  }, []);

  return (
    <DefaultLayoutStyled>
      <Seo {...seo} />
      <Header />
      {children}
      <Footer />
      {showScrollToTop && (
        <button
          onClick={() => animateScroll.scrollTo(0)}
          className="noStyle ar-btn-scrollToTop"
        >
          <Image
            width={48}
            height={48}
            src="/images/icon_scroll.svg"
            alt="icon"
          />
        </button>
      )}
    </DefaultLayoutStyled>
  );
};

export default memo(DefaultLayout);
