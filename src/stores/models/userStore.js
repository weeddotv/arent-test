import handleError from '@/utils/handleError';
import api from '@/utils/api';

const initState = {
  auth: {},
  achievement: {},
  bodyWeight: [],
  mealHistory: {
    items: [],
    pagination: {
      current: 1,
      pageSize: 8,
      total: 0,
    },
  },
  myExercise: {
    items: [],
    pagination: {
      current: 1,
      pageSize: 8,
      total: 0,
    },
  },
  myDiary: {
    items: [],
    pagination: {
      current: 1,
      pageSize: 8,
      total: 0,
    },
  },
};

const UserStore = {
  state: initState,
  reducers: {
    setAchievement(state, achievement) {
      return {
        ...state,
        achievement,
      };
    },
    setBodyWeight(state, bodyWeight) {
      return {
        ...state,
        bodyWeight,
      };
    },
    setMealHistory(state, mealHistory) {
      return {
        ...state,
        mealHistory,
      };
    },
    setMyExercise(state, myExercise) {
      return {
        ...state,
        myExercise,
      };
    },
    setMyDiary(state, myDiary) {
      return {
        ...state,
        myDiary,
      };
    },
    addMoreMealHistory(state, payload) {
      return {
        ...state,
        mealHistory: {
          ...payload,
          items: state.mealHistory.items.concat(payload.items),
        },
      };
    },
    addMoreMyDiary(state, payload) {
      return {
        ...state,
        myDiary: {
          ...payload,
          items: state.myDiary.items.concat(payload.items),
        },
      };
    },
  },
  effects: (_dispatch) => ({
    async getAchievement() {
      try {
        const { data } = await api.get(`/user/achievement`);
        this.setAchievement(data);
      } catch (error) {
        handleError(error);
      }
    },
    async getBodyWeight() {
      try {
        const { data } = await api.get(`/user/body-weight`);
        this.setBodyWeight(data);
      } catch (error) {
        handleError(error);
      }
    },
    async getMealHistory(payload) {
      try {
        const { pagination, filters } = payload || {};
        const params = {
          current: pagination?.current,
          pageSize: pagination?.pageSize,
          ...filters,
        };

        const { data } = await api.get(`/user/meal-history`, { params });
        this.setMealHistory(data);
      } catch (error) {
        handleError(error);
      }
    },
    async getMyDiary(payload) {
      try {
        const { pagination, filters } = payload || {};
        const params = {
          current: pagination?.current,
          pageSize: pagination?.pageSize,
          ...filters,
        };

        const { data } = await api.get(`/user/my-diary`, { params });
        this.setMyDiary(data);
      } catch (error) {
        handleError(error);
      }
    },
    async getMyExercise() {
      try {
        const { data } = await api.get(`/user/my-exercise`);
        this.setMyExercise(data);
      } catch (error) {
        handleError(error);
      }
    },
    async loadMoreMealHistory(payload) {
      try {
        const { pagination, filters } = payload || {};
        const params = {
          current: pagination?.current,
          pageSize: pagination?.pageSize,
          ...filters,
        };
        const { data } = await api.get(`/user/meal-history`, { params });
        this.addMoreMealHistory(data);
      } catch (error) {
        handleError(error);
      }
    },
    async loadMoreMyDiary(payload) {
      try {
        const { pagination, filters } = payload || {};
        const params = {
          current: pagination?.current,
          pageSize: pagination?.pageSize,
          ...filters,
        };
        const { data } = await api.get(`/user/my-diary`, { params });
        this.addMoreMyDiary(data);
      } catch (error) {
        handleError(error);
      }
    },
  }),
  selectors: (_slice, _createSelector, _hasProps) => ({}),
};

export default UserStore;
