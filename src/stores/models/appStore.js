import handleError from '@/utils/handleError';
import { MODAL_TYPE } from '../../utils/constants';
import api from '@/utils/api';

const initState = {
  columns: {
    items: [],
    pagination: {
      current: 1,
      pageSize: 8,
      total: 0,
    },
  },
};

const AppStore = {
  state: initState,
  reducers: {
    setColumns(state, columns) {
      return {
        ...state,
        columns,
      };
    },
    addMoreColumns(state, payload) {
      return {
        ...state,
        columns: {
          ...payload,
          items: state.columns.items.concat(payload.items),
        },
      };
    },
  },
  effects: (_dispatch) => ({
    async getColumns(payload) {
      try {
        const { pagination, filters } = payload || {};
        const params = {
          current: pagination?.current,
          pageSize: pagination?.pageSize,
          ...filters,
        };

        const { data } = await api.get(`/columns`, { params });
        this.setColumns(data);
      } catch (error) {
        handleError(error);
      }
    },
    async loadMoreColumns(payload) {
      try {
        const { pagination, filters } = payload || {};
        const params = {
          current: pagination?.current,
          pageSize: pagination?.pageSize,
          ...filters,
        };
        const { data } = await api.get(`/columns`, { params });
        this.addMoreColumns(data);
      } catch (error) {
        handleError(error);
      }
    },
  }),
  selectors: (_slice, _createSelector, _hasProps) => ({}),
};

export default AppStore;
