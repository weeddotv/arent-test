import { init } from "@rematch/core";
import loadingPlugin from "@rematch/loading";

import * as models from "./models";

const store = init({
  plugins: [loadingPlugin()],
  models,
});

export default store;
