import axios from 'axios';
const api = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_ENDPOINT}/api`,
});

api.interceptors.request.use(function (config) {
  if (localStorage) {
  }
  return config;
});

export default api;
