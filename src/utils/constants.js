const STORAGE_KEY = {
  AUTH_TOKEN: 'auth-token',
};

const ROUTE = {
  TOP: '/',
  MY_RECORD: '/my-record',
  COLUMN: '/column',
  LOGIN: '/login',
};

const MODAL_TYPE = {
  ERROR: 'error',
  INFO: 'info',
  SUSCCESS: 'success',
  CONFIRM: 'confirm',
};

const MEAL_HISTORY_TYPE = {
  MORNING: 'Morning',
  LUNCH: 'Lunch',
  DINER: 'Dinner',
  SNACK: 'Snack',
};


const MEDIA_SIZE = {
  TABLET: 961,
  DESKTOP: 1280,
};

export { STORAGE_KEY, ROUTE, MODAL_TYPE, MEDIA_SIZE, MEAL_HISTORY_TYPE };
